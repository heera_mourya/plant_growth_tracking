# Introduction

- Based on NDVI( Normalized Difference Vegetation Index) we calculate how the vegetation changes month to month.

- Dataset download by AWS s3 service as an API for that we need AWS_access key and AWS_secret key.

- Sentinel-2 satellite used for downloading different corresponding bands.

- We also need scihub.copernicus credentials.
- Path is defined as a folder where we are putting our all files. 

